# HapPy User Guide

HapPy (Haploid Selection Mapping in Python) is a computational pipeline that allows fast and efficient analysis of the results of a Haploid Selection Mapping experiment. This guide will walk you through how to use the individual scripts within the program.

## A couple of guidelines before we start

 * Back up files before messing with them. As far as I can tell, everything in the pipeline is functional, so if something breaks it's good to have something you can go back to.
 * Keep all of the files in the same directory (folder), since the scripts have to be in the same directory as the files they access.
 * The scripts are all documented individually, so if something in this guide is confusing or seems mysterious, feel free to open the scripts in a text editor and see what's in the comments.

Now let's get HapPy!


## What HapPy needs:

 * Three 'reference' BAM files from the accessions you crossed and from the accession you used as the male-sterile F1 female.
 * Two BED files containing SNP information between Accession 1 and 2, and Accession 1 and 3. It doesn't matter what accession is which, but the one that is compared twice has to be first!
 * One 'CrossBAM' that is your sequencing results from the HSM experiment.
 * And, naturally, the scripts in HapPy.

All of these should be in the same folder/directory so that the scripts can access them all!

## How to use HapPy:

### BedCombiner

First step is to combine your two BED files into a Combined_BED that will contain all SNP information between the three accessions involved in your experiment. Do this by replacing the indicated lines in BedCombiner.py at the very bottom to the names of your BED files. Then run BED_Combiner.sh in the terminal and the program will spit out a Combined_BED.txt.

### HomozygoteFinder

Next, the program will filter your Combined_BED into a file containing only SNPs located at sites that are statistically likely to be homozygous in all three accessions. This is done to simplify the math in the following step, as homozygous loci are less subject to intra-accession variation and introduce an extra element of variation into the Chi Square in the next step. To use HomozygoteFinder, plug in your reference BAM files in the spots where it says to. If you changed the name from the last step (from Combined_BED.txt)make sure to update that too. Lastly, you can switch around the statistical test in the middle of the program (where it's marked) to fine-tune its Homozygote-Finding abilities. Run HomozygoteFinder.sh in the terminal to start.

As a sub-action of this, because of the use of multithreading, the program will output sixteen different files. In order to combine them into one BED file, run MasterBedSorter.py without touching anything.

### HaploidSelectionMap

Now it's time to filter the resulting BED down to the sites showing significant selection. There are a couple of things you can change up in this script. The most obvious is switching around the test in the middle of the program. It's currently using a Chi-square test with a limit on p-value and ratio; it's explained better in the program, but change the parameters as you see fit. Elsewise, change around the files it uses as you see fit--you'll need to give it your Cross BAM and the BED file that resulted from HomozygoteFinder.py. Run HaploidSelectionMap.sh in the terminal to start.
Same deal as the previous part of the program--run ResultsCombiner.py to combine all the sub-files into Results.txt.

### SlidingWindow

Lastly, to better filter results, run SlidingWindow.sh to search through your results from HaploidSelectionMap.py. It'll give you a list of positions that are the starts of 'windows' of selection, which are runs of positions that are all pointing in the same direction as far as selection. This is the pattern you'd expect to see if a genetic block was being selected for or against, rather than just noise. You can adjust two things: the length of the window you look at, and how many mistakes you allow in the window before you call it noise. Look in the program at how to change it!

## Contact
If you have any questions or need to get into contact, feel free to email me at liam_oconnell@brown.edu. 

Thanks for reading, and good luck with HapPy!